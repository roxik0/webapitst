﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Security.Cryptography.X509Certificates;

namespace WebApiTest.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;
            
            Menu menu = new MainMenu();
            menu.Show();
        }

    }
}
