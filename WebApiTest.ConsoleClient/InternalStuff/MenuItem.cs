﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class MenuItem:Control
    {
        public string Title { get; set; }
        public bool Selected { get; set; }
        public Action Action { get; set; }

      

        public void Execute()
        {
            Action?.Invoke();
        }

        public override void Draw()
        {
            Console.CursorLeft = Left;
            Console.CursorTop = Top;
            if (Selected)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Blue;
            }

            Console.Write($"|{Title.PadRight(Width)}|");
            Console.BackgroundColor = ConsoleColor.Blue;
        }

        public override void ProcessUserInput()
        {
            throw new NotImplementedException();
        }
    }
}