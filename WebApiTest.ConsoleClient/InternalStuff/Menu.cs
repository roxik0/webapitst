﻿using System;
using System.Collections.Generic;

namespace WebApiTest.ConsoleClient
{
    public class Menu:Control
    {
      

        protected List<MenuItem> _menuItems = new List<MenuItem>();
        protected int selectedIndex = 0;

       

        private void HandleMenu()
        {
            int oldSelected = selectedIndex;
            var key = Console.ReadKey();
            if (key.Key == ConsoleKey.UpArrow)
            {

                if (selectedIndex - 1 < 0)
                {
                    selectedIndex = 0;
                }
                else
                {
                    selectedIndex--;
                }
            }

            if (key.Key == ConsoleKey.DownArrow)
            {
                if (selectedIndex + 1 >= _menuItems.Count)
                {
                    selectedIndex = _menuItems.Count - 1;
                }
                else
                {
                    selectedIndex++;
                }

            }

            if (key.Key == ConsoleKey.Enter)
            {
                _menuItems[selectedIndex].Execute();
            }

            _menuItems[oldSelected].Selected = false;
            _menuItems[selectedIndex].Selected = true;
            NeedRedraw = true;
        }

        private void DrawMenu()
        {
            var oldColor = Console.BackgroundColor;
            Console.CursorLeft = Left;
            Console.CursorTop = Top;
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write(new string('-', Width));
            int i;
            for (i = 0; i < _menuItems.Count; i++)
            {
                var menuItem = _menuItems[i];
                menuItem.Top = Top + i+1;
                menuItem.Left = Left;
                menuItem.Width = Width - 2;
                menuItem.Draw();
            }

            Console.CursorLeft = Left;
            Console.CursorTop = Top+i + 1;
            Console.Write(new string('-', Width));
            Console.BackgroundColor = oldColor;
        }

        protected void AddMenuItem(string title, Action action)
        {
            _menuItems.Add(new MenuItem() {Title = title, Action = action});
        }

        public override void Draw()
        {
            DrawMenu();
               
            
        }

        public override void ProcessUserInput()
        {
            HandleMenu();
        }

       
    }
}