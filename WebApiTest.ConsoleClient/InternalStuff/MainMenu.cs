﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class MainMenu : Menu
    {
        public MainMenu()
        {
            Width = 40;
            Left = Console.BufferWidth / 2 - Width / 2;
            Top = Console.WindowHeight / 2 - Height / 2;
            base.AddMenuItem("Instrukcja", Instruction);
            base.AddMenuItem("Uruchom Testy", RunTests);
            base.AddMenuItem("Exit", Exit);
        }



        private void Instruction()
        {
            var instructionModal = new InstructionModal();
            instructionModal.Show();
        }

        private void RunTests()
        {
            var testsModal= new TestsModal();
            testsModal.Show();
        }

        private void Exit()
        {
            IsActive = false;
        }
    }

    public enum TestResult
    {
        NotRun,
        Running,
        Passed,
        Failed
    }
}