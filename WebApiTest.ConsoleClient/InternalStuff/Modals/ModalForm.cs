﻿using System;
using System.Collections.Generic;

namespace WebApiTest.ConsoleClient
{
    public class ModalForm:Control
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public List<Control> Controls { get; set; } = new List<Control>();
        public ModalForm()
        {
            

        }

        


        public Control FocusedControl { get; set; }

        private void DrawControls()
        {
            int top = Top + 1;
            foreach (var control in Controls)
            {
                control.Top = top;
                top += control.Height;
                control.Left = Left+1;
                control.Draw();
              
            }

        }

        private void DrawModalFrame()
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.CursorTop = Top;
            Console.CursorLeft = Left;
            Console.Write(new string('-', Width));
            for (var i = 0; i < Height-2; i++)
            {
                Console.CursorTop = Top+ i + 1;
                Console.CursorLeft = Left;
                Console.Write('|');
                Console.Write(new string(' ', Width-2));
                Console.Write('|');
            }

            Console.CursorTop = Top+Height-1;
            Console.CursorLeft = Left;
            Console.Write(new string('-', Width));
        }

        protected void Close()
        {
            Console.ResetColor();
            Console.Clear();
            IsActive = false;
        }

        public override void Draw()
        {

            DrawModalFrame();
            DrawControls();
        }

        public override void ProcessUserInput()
        {
            FocusedControl?.ProcessUserInput();
        }
    }
}