﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApiTest.ConsoleClient
{
    public class TestsModal : ModalForm
    {
        public List<TestExecutor> TestsList { get; set; }
        public TestsModal()
        {
            Width = 90;
            Height = 20;
            Left = Console.BufferWidth / 2 - Width / 2;
            Top = Console.WindowHeight / 2 - Height / 2;

            TestsList = new List<TestExecutor>();
            TestsList.Add( new TestExecutor(){TestName = "Simple Get", Action = Tests.GetExample });
            TestsList.Add(new TestExecutor() { TestName = "Post Cat", Action = Tests.Post });
            TestsList.Add(new TestExecutor() { TestName = "Delete Cat", Action = Tests.Delete });
            TestsList.Add(new TestExecutor() { TestName = "Get All Cats", Action = Tests.GetAll });
            TestsList.Add(new TestExecutor() { TestName = "Update Cat", Action = Tests.Put });
            Controls.Add(new TextBlock() { Text = "Wciśnij Enter aby rozpocząć"});
            Controls.AddRange(TestsList);
            Controls.Add(new Button() { Text = "RunTests", Action =RunTests });
            FocusedControl = Controls.Last();
        }

        private void RunTests()
        {
            foreach (var testExecutor in TestsList)
            {
                testExecutor.RunTest();
            }
        }
    }
}