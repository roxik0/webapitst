﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class InstructionModal : ModalForm
    {
        public InstructionModal()
        {
            Width = 90;
            Height = 20;
            Left = Console.BufferWidth / 2 - Width / 2;
            Top = Console.WindowHeight / 2 - Height / 2;

            Controls.Add(new TextBlock()
            {
                Text =
                    @"Drodzy Kursanci,
Witam na moim teście celem jest stworzenie kontrolera WebApi oraz klienta,
który rozwiąże testy zawarte w pliku Tests.cs 
Metody  które macię edytować mają not implemented exception).
Kod controllera możecie modyfikować dowolnie :) 
Bawcie się dobrze czas na zadanie to 60 minut, ale powinniście dać radę szybciej.
W komentarzach w metodach są adresy url. Punktacja:
za każdą metodę 15% metod jest 5 => 75%
za prawidłową i ładną implementację klienta WebApi => 10%
za jakość kodu  => 15%

PS. Po uruchomieniu przykładowy test powinien zaświecić się na zielono. 
Pamiętajcie by uruchamiać projekt webApi jako PROJECT nie IIS
",
                Height = 14,
            });
            Controls.Add(new Button() {Text = "Ok", Action = () => base.Close()});
            FocusedControl = Controls[1];
        }
    }
}