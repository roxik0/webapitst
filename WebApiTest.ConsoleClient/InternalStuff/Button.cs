﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class Button:Control
    {
        public string Text { get; set; }
        public Action Action { get; set; }
        public override void Draw()
        {
            var oldColor = Console.BackgroundColor;
            Console.SetCursorPosition(Left,Top);
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.Write(Text);
            Console.BackgroundColor = oldColor;
        }

        public override void ProcessUserInput()
        {
            var key= Console.ReadKey(false);
            if (key.Key == ConsoleKey.Enter)
            {
                Action?.Invoke();
            }
        }

       
    }
}