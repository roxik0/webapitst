﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class TextBlock : Control
    {
        public override void Draw()
        {
            Console.CursorTop = Top;
            Console.CursorLeft = Left;
            var lines = Text.Split(Environment.NewLine);
            foreach (var line in lines)
            {
                Console.CursorLeft = Left;
               
                Console.Write(line);
                Console.CursorTop++;
            }
          
        }

        public override void ProcessUserInput()
        {
            throw new NotImplementedException();
        }

        

        public string Text { get; set; }
    }
}