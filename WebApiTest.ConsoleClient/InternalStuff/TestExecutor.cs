﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace WebApiTest.ConsoleClient
{
    public class TestExecutor : Control
    {
        private string _testName;

        public string TestName
        {
            get => _testName;
            set
            {
                _testName = value;
                Text = value;
            }
        }

        public string Text { get; set; }
        public Func<bool> Action { get; set; }

        public override void Draw()
        {
           
            
                var oldColor = Console.BackgroundColor;
                switch (TestResult)
                {
                    case TestResult.NotRun:
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        break;
                    case TestResult.Running:
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        break;
                    case TestResult.Passed:
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        break;
                    case TestResult.Failed:
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        break;
                }

                Console.SetCursorPosition(Left, Top);
                Console.Write(Text.PadRight(80));
                Console.BackgroundColor = oldColor;
            
        }

        public override void ProcessUserInput()
        {
            Console.ReadKey();
        }

        public TestResult TestResult { get; set; }

      

        public void RunTest()
        {
            ExecuteTest();
            Text = TestName;
        }

        private void ExecuteTest()
        {
            TestResult = TestResult.Running;
            Draw();
            try
            {
                var result = Action?.Invoke();

                if (result == true)
                {
                    TestResult = TestResult.Passed;
                }
                else
                {
                    TestResult = TestResult.Failed;
                }
            }
            catch (Exception exception)
            {
                //If Any exception fail
                TestResult = TestResult.Failed;
                if (exception.Message.Length < 65)
                {
                    Text += " -> " + exception.Message;

                }
                else
                {
                    Text += " -> " + exception.Message.Substring(0, 65);

                }

            }

            Draw();
           
        }

      
    }
}