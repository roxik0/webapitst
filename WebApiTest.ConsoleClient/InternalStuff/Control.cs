﻿using System;

namespace WebApiTest.ConsoleClient
{
    public abstract class Control
    {
        public abstract void Draw();

        private void InternalDraw()
        {
            if (NeedRedraw)
            {
                Draw();
                NeedRedraw = false;
            }
        }

        public bool NeedRedraw { get; set; } = true;


        public void Show()
        {
            while (IsActive)
            {
                InternalDraw();
                ProcessUserInput();
            }
        }

        protected bool IsActive { get; set; } = true;

        public abstract void ProcessUserInput();
        public int Width { get; set; }
        public int Top { get; set; }
        public int Height { get; set; } = 1;
        public int Left { get; set; }

       
    }
}