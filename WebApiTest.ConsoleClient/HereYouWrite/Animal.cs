﻿using System;

namespace WebApiTest.ConsoleClient
{
    public class Animal
    {
        public string Name { get; set; }
        public string Type { get; set; }

        protected bool Equals(Animal other)
        {
            return Name == other.Name && Type == other.Type && Age == other.Age;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Animal) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Type, Age);
        }

        public static bool operator ==(Animal left, Animal right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Animal left, Animal right)
        {
            return !Equals(left, right);
        }

        public int Age { get; set; }
    }
}