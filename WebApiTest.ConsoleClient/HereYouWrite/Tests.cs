﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;

namespace WebApiTest.ConsoleClient
{
    public class Tests
    {
        public static bool GetExample()
        {
            HttpClient client=new HttpClient();
            return client.GetAsync(new Uri("http://localhost:9115/simple")).Result.Content.ReadAsStringAsync().Result == "1";
            
        }
        public static bool Post()
        {
            var animal= new Animal()
            {
                Name = "Mruczek",
                Age = 2,
                Type = "Cat",
            };
            PostAnimal(animal);
            var responseAnimal = GetAnimal("Mruczek");
            DeleteAnimal("Mruczek");
            return responseAnimal == animal;

        }
       
        public static bool Delete()
        {
            var animal = new Animal()
            {
                Name = "Mruczek",
                Age = 2,
                Type = "Cat",
            };
            PostAnimal(animal);
            DeleteAnimal("Mruczek");
            var responseAnimal=GetAnimal("Mruczek");
            return animal == null;
        }
        public static bool Put()
        {
            var animal = new Animal()
            {
                Name = "Mruczek",
                Age = 2,
                Type = "Cat",
            };
            PostAnimal(animal);
            animal.Age = 5;
            PutAnimal(animal);
            var responseAnimal = GetAnimal("Mruczek");
            DeleteAnimal("Mruczek");
            return responseAnimal.Age ==5;
        }
        public static bool GetAll()
        {
            var animal = new Animal()
            {
                Name = "Mruczek",
                Age = 2,
                Type = "Cat",
            };
            PostAnimal(animal);
            animal.Name = "Filemon";
            PostAnimal(animal);
            animal.Name = "Kicek";
            PostAnimal(animal);
            var responseAnimals = GetAllAnimals();
            return responseAnimals.Count() == 3;
        }

        private static IEnumerable<Animal> GetAllAnimals()
        {
            //http://localhost:9115/animals
            throw new NotImplementedException();
        }


        private static void PutAnimal(Animal animal)
        {
            //http://localhost:9115/animals/{name}
            throw new NotImplementedException();
        }

        private static Animal GetAnimal(string name)
        {
            //http://localhost:9115/animals/{name}
            throw new NotImplementedException();
        }

        private static void DeleteAnimal(string name)
        {
            //http://localhost:9115/animals/{name}
            throw new NotImplementedException();
        }

        private static void PostAnimal(Animal animal)
        {
            //http://localhost:9115/animals/
            throw new NotImplementedException();
        }

        
    }
}